extends Spatial

export(bool) var start_VR = true
export(int,0,1) var player_id = 0

var current_node:Sprite = null
var scene_path = ""
var scene:Spatial = null

func _ready():
	
	$osc_sender.pid = player_id
	
	if start_VR:
		
		remove_child( $cam_pivot )
		$ui.visible = false
		$ARVROrigin.visible = true
		$ARVROrigin/Player_Camera.visible = true
		$ARVROrigin/Player_Camera.current = true
		
		# We will be using OpenVR to drive the VR interface, so we need to find and initialize it.
		var VR = ARVRServer.find_interface("OpenVR")
		if VR and VR.initialize():
			
			# Turn the main viewport into a AR/VR viewport,
			# and turn off HDR (which currently does not work)
			get_viewport().arvr = true
			get_viewport().hdr = false
			
			# Let's disable VSync so we are not running at whatever the monitor's VSync is,
			# and let's set the target FPS to 90, which is standard for most VR headsets.
			OS.vsync_enabled = false
			Engine.target_fps = 90
			# Also, the physics FPS in the project settings is also 90 FPS. This makes the physics
			# run at the same frame rate as the display, which makes things look smoother in VR!
	else:
		remove_child( $ARVROrigin )
		$cam_pivot.visible = true
		$cam_pivot/cam.visible = true
		$cam_pivot/cam.current = true

func get_player():
	if start_VR:
		return $ARVROrigin/Player_Camera
	else:
		return $cam_pivot/cam

func set_not_me( pos:Vector3, rot:Vector3 ):
	$not_me.translation = pos
	$not_me.rotation = rot
	$ui/grid/dist_pos.text = str(int(pos.x*100)*0.01) + ',' + str(int(pos.y*100)*0.01) + ',' + str(int(pos.z*100)*0.01)

func update_tree( pid:int, path:String ):
	var n:Sprite = get_node(path)
	$ui/tree.set_player( pid, n )
	$ui/grid/dist_scene.text = n.scene

func do_teleport():
	if scene != null:
		remove_child( scene )
	scene = null

func _process(delta):
	if scene == null:
		init_scene()
	if !start_VR:
		var p:Vector3 = $cam_pivot/cam.global_transform.origin
		$ui/grid/pos.text = str(int(p.x*100)*0.01) + ',' + str(int(p.y*100)*0.01) + ',' + str(int(p.z*100)*0.01)

func init_scene():
	
	if current_node == null:
		current_node = $ui/tree.get_target()
	else:
		current_node = current_node.get_target( player_id )
	
	if current_node == null:
		$ui/grid/scene.text = "ERROR"
		return
	
	$ui/tree.set_player( player_id, current_node )
	$osc_sender.path = current_node.get_path()
	current_node.preload_targets()
	
	scene_path = current_node.scene_folder + current_node.scene
	$ui/grid/scene.text = current_node.scene
	
	var sc = load( scene_path )
	if sc == null:
		get_tree().quit()
	scene = sc.instance()
	add_child( scene )
	
	if start_VR: 
		scene.player = $ARVROrigin/Player_Camera
	else:
		scene.player = $cam_pivot/cam
	scene.connect( 'teleport', self, 'do_teleport' )
