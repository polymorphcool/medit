extends "handfx_common.gd"

export (Vector3) var noise_mult:Vector3 = Vector3(1,1,1)
export (float,0,10) var noise_strength:float = 1
export (bool) var noise_local:bool = false

func _ready():
	fx_initialise()

# warning-ignore:unused_argument
func _process(delta):
	if !fx_ready():
		return
	for i in range(0,len(trackers)):
		if !mask[i]:
			continue
		var t = trackers[i]
		var n:Vector3 = Vector3(
			rand_range(-noise_strength,noise_strength),
			rand_range(-noise_strength,noise_strength),
			rand_range(-noise_strength,noise_strength)) * noise_mult
		if noise_local:
			n = transforms[i].basis.xform(n)
		if previous[i] != null:
			t.translation += n - previous[i]
		previous[i] = n
