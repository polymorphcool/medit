extends MeshInstance

export (Vector2) var uv_speed:Vector2 = Vector2(0,1)

func _process(delta):
	material_override.uv1_offset.x += uv_speed.x * delta
	material_override.uv1_offset.y += uv_speed.y * delta
