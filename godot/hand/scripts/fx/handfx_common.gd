extends Node

export (NodePath) var hand:NodePath = ""

export (bool) var play:bool = false
export (bool) var palm:bool = false
export (bool) var thumb:bool = true
export (bool) var index:bool = true
export (bool) var middle:bool = true
export (bool) var ring:bool = true
export (bool) var pinky:bool = true

var h:Spatial = null
var trackers:Array = []
var transforms:Array = []
var previous:Array = []
onready var mask:Array = [palm,thumb,index,middle,ring,pinky]

func fx_initialise():
	h = get_node( hand )
	if h != null and !h.has_method( 'get_trackers' ):
		h = null
	if h != null:
		trackers = h.get_trackers()
		for i in range(0,len(trackers)):
			var t = trackers[i]
			transforms.append( Transform(t.transform) )
			previous.append( null )

func fx_ready():
	return play and h != null and h.visible
