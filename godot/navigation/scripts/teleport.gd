extends MeshInstance

export(NodePath) var tracked : NodePath = ""
export(NodePath) var world : NodePath = ""
export(NodePath) var target : NodePath = ""
export(float) var radius : float = 1
export(Color) var inactive_color : Color = Color(0.3,0.3,0.3,1)
export(Color) var active_color : Color = Color(1,1,1,1)

var _tracked : Spatial = null
var _world : Spatial = null
var _target : Spatial = null
var active : bool = true
var distance : float = 0

func _ready():
	_tracked = get_node(tracked)
	_world = get_node(world)
	_target = get_node(target)
	if _target != null and !_target.has_method("teleport"):
		print( "_target must have a teleport() method!" )
		_target = null
	material_override = material_override.duplicate()
	$halo_in.material_override = $halo_in.material_override.duplicate()
	$halo_out.material_override = $halo_in.material_override
	$halo_in.material_override.set_shader_param("alpha", 0.0)

func teleport():
	# deactivation of the teleport
	active = false
	material_override.albedo_color = inactive_color
	$halo_in.material_override.set_shader_param("alpha", 0.0)

# warning-ignore:unused_argument
func _process(delta):
	if _tracked == null:
		return
	# do not consider elevation (Y axis)
	var diff : Vector3 = (global_transform.origin - _tracked.global_transform.origin)*Vector3(1,0,1)
	distance = diff.length()
	# reactivation
	if !active:
		if distance > radius:
			active = true
		return
	if _target == null:
		return
	if distance < radius * 0.5:
		# moving the world
		_world.global_transform.origin += global_transform.origin - _target.global_transform.origin
		material_override.albedo_color = inactive_color
		# call the target
		_target.teleport()
	else:
		var pc : float = min(1, max( 0, ( distance / radius ) - 0.5 ))
		material_override.albedo_color = active_color.linear_interpolate( inactive_color, pc )
		$halo_in.material_override.set_shader_param("alpha", (1-pc))
