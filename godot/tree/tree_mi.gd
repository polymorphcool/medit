tool

extends MeshInstance

export (bool) var generate:bool = false setget set_generate
export (bool) var fast:bool = true setget set_fast
export (bool) var leaves:bool = false setget set_leaves

export (int,0,10) var depth:int = 6 setget set_depth
export (int,0,10) var split:int = 3 setget set_split
export (float,0,1) var dead_branches:float = 0.1 setget set_dead_branches
export (float,0,90) var split_angle:float = 25 setget set_split_angle
export (float,0,1) var split_deviation:float = 0.1 setget set_split_deviation

export (Curve) var length:Curve = null
export (Curve) var split_count:Curve = null

export (float,0,1) var length_deviation:float = 0.5 setget set_length_deviation
export (float,0,1) var thickness_bottom:float = 0.3 setget set_thickness_bottom
export (float,0,1) var thickness_top:float = 0.1 setget set_thickness_top
export (int,3,24) var trunk_definition:int = 6 setget set_trunk_definition
export (int,0,10) var leave_start:int = 2 setget set_leave_start
export (float,0,4) var leave_scale:float = 0.1 setget set_leave_scale
export (float,0,4) var leave_offset:float = 0.5 setget set_leave_offset
export (float,-1,1) var leave_up:float = 0.8 setget set_leave_up
export (float,-1,1) var leave_up_noise:float = 0.3 setget set_leave_up_noise
export (float,0,1) var leave_link:float = 0.1 setget set_leave_link

export (Material) var trunk_mat:Material = null
export (Material) var leaves_mat:Material = null
export (Material) var leaflinks_mat:Material = null

func set_generate( b:bool ):
	generate = false
	if b:
		build()

func set_fast(b:bool):
	fast = b
	if fast:
		wireframe()
	else:
		build_mesh()

func set_leaves(b:bool):
	leaves = b
	if !fast:
		build_mesh()

func set_depth( i:int ):
	depth = i
	if fast:
		build()

func set_split( i:int ):
	split = i
	if fast:
		build()

func set_dead_branches( f:float):
	dead_branches = f
	if fast:
		build()

func set_split_angle( f:float ):
	split_angle = f
	if fast:
		build_splits()
		build_positions()
		wireframe()

func set_split_deviation( f:float ):
	split_deviation = f
	if fast:
		build_splits()
		build_positions()
		wireframe()

func set_length_deviation( f:float ):
	length_deviation = f
	if fast:
		build_length()
		build_positions()
		wireframe()

func set_thickness_bottom( f:float ):
	thickness_bottom = f
	build_mesh()

func set_thickness_top( f:float ):
	thickness_top = f
	build_mesh()

func set_trunk_definition( i:int ):
	trunk_definition = i
	build_mesh()

func set_leave_start(i:int):
	leave_start = i
	if leaves:
		build_mesh()

func set_leave_scale(f:float):
	leave_scale = f
	if leaves:
		build_mesh()

func set_leave_offset(f:float):
	leave_offset = f
	if leaves:
		build_mesh()

func set_leave_up(f:float):
	leave_up = f
	if leaves:
		build_mesh()

func set_leave_up_noise(f:float):
	leave_up_noise = f
	if leaves:
		build_mesh()

func set_leave_link(f:float):
	leave_link = f
	if leaves:
		build_mesh()

var branches:Array = []
var max_length:float = 0
var fast_imm:ImmediateGeometry = null
var fast_mat:SpatialMaterial = null

# trunk and leaves arrays
var t_verts:PoolVector3Array
var t_norms:PoolVector3Array
var t_tangents:PoolRealArray
var t_uvs:PoolVector2Array
var l_verts:PoolVector3Array
var l_norms:PoolVector3Array
var l_tangents:PoolRealArray
var l_uvs:PoolVector2Array
var b_verts:PoolVector3Array
var b_norms:PoolVector3Array
var b_tangents:PoolRealArray
var b_uvs:PoolVector2Array

func branch_new( parent, level:int ):
	var b = {
		'parent': parent,
		'children': [],
		'basis': Basis.IDENTITY,
		'children_rot': randf()*TAU,
		'bottom': Vector3.ZERO,
		'top': Vector3.ZERO,
		'length': 0,
		'total_length': 0,
		'thickness': 0,
		'level': level,
		'vertices': null,
		'normals': null,
		'tangents': null
	}
	branches.append(b)
	return b

func branches_split( bs:Array ):
	var out = []
	for b in bs:
		var snum = int(max(1,split_count.interpolate( b.level * 1.0 / depth )))
		for i in range( 0, snum ):
			var newb = branch_new( b, b.level+1 )
			b.children.append( newb )
			out.append( newb )
	return out

func build_hierarchy():
	branches = []
	var trunk = branch_new( null, 0 )
	var lvl_branches = [trunk]
	for i in range(0,depth):
		randomize()
		if randf() < dead_branches:
			continue 
		lvl_branches = branches_split( lvl_branches )

func build_splits():
	for b in branches:
		var cnum:int = len(b.children)
		if cnum == 0:
			continue
		var brot = b.children_rot
		var agap = TAU / cnum
		for i in range(0,cnum):
			var a = brot + i * agap + agap * rand_range( -split_deviation, split_deviation )
			var spa = split_angle + split_angle * rand_range( -split_deviation, split_deviation )
			var newdir = b.basis.y.slerp( ( cos(a) * b.basis.x + sin(a) * b.basis.z ).normalized(), split_angle / 90 ).normalized()
			var z = newdir.cross(b.basis.x).normalized()
			var x = z.cross(newdir).normalized()
			b.children[i].basis = Basis(x,newdir,z)

func build_length():
	for b in branches:
		b.length = length.interpolate( b.level * 1.0 / depth )
		b.length += rand_range(-length_deviation,length_deviation)

func build_positions():
	max_length = 0
	for b in branches:
		if b.parent == null:
			b.bottom = Vector3.ZERO
			b.total_length = b.length
		else:
			b.bottom = b.parent.top
			b.total_length = b.parent.total_length
		b.top = b.bottom + b.basis.y * b.length
		b.total_length += b.length
		if max_length < b.total_length:
			max_length = b.total_length

func build_branch(ugap:float, i:int, j:int, top:Dictionary, bottom:Dictionary):
	
	var uj:int = i+1
	var bh = max_length - (top.total_length - top.length)
	var th = max_length - top.total_length
	
	t_uvs.push_back( Vector2(ugap*i,th) )
	t_norms.push_back( top.normals[i] )
	t_tangents.push_back( top.tangents[i].x )
	t_tangents.push_back( top.tangents[i].y )
	t_tangents.push_back( top.tangents[i].z )
	t_tangents.push_back( 1 )
	t_verts.push_back( top.vertices[i] )
	###### V
	t_uvs.push_back( Vector2(ugap*uj,th) )
	t_norms.push_back( top.normals[j] )
	t_tangents.push_back( top.tangents[j].x )
	t_tangents.push_back( top.tangents[j].y )
	t_tangents.push_back( top.tangents[j].z )
	t_tangents.push_back( 1 )
	t_verts.push_back( top.vertices[j] )
	###### V
	t_uvs.push_back( Vector2(ugap*i,bh) )
	t_norms.push_back( bottom.normals[i] )
	t_tangents.push_back( bottom.tangents[i].x )
	t_tangents.push_back( bottom.tangents[i].y )
	t_tangents.push_back( bottom.tangents[i].z )
	t_tangents.push_back( 1 )
	t_verts.push_back( bottom.vertices[i] )

	t_uvs.push_back( Vector2(ugap*uj,bh) )
	t_norms.push_back( bottom.normals[j] )
	t_tangents.push_back( bottom.tangents[j].x )
	t_tangents.push_back( bottom.tangents[j].y )
	t_tangents.push_back( bottom.tangents[j].z )
	t_tangents.push_back( 1 )
	t_verts.push_back( bottom.vertices[j] )
	###### V
	t_uvs.push_back( Vector2(ugap*i,bh) )
	t_norms.push_back( bottom.normals[i] )
	t_tangents.push_back( bottom.tangents[i].x )
	t_tangents.push_back( bottom.tangents[i].y )
	t_tangents.push_back( bottom.tangents[i].z )
	t_tangents.push_back( 1 )
	t_verts.push_back( bottom.vertices[i] )
	###### V
	t_uvs.push_back( Vector2(ugap*uj,th) )
	t_norms.push_back( top.normals[j] )
	t_tangents.push_back( top.tangents[j].x )
	t_tangents.push_back( top.tangents[j].y )
	t_tangents.push_back( top.tangents[j].z )
	t_tangents.push_back( 1 )
	t_verts.push_back( top.vertices[j] )

func build_tail( start:Vector3, end:Vector3 ):
	
	var fwd = (end-start).normalized()
	var up = Vector3.RIGHT.cross(fwd)
	var right = fwd.cross(up)
	
	b_uvs.push_back( Vector2(0,0) )
	b_norms.push_back( up )
	b_verts.push_back( start + right * leave_link * 0.5 )
	###### V
	b_uvs.push_back( Vector2(0,0) )
	b_norms.push_back( up )
	b_verts.push_back( start + right * leave_link * -0.5 )
	###### V
	b_uvs.push_back( Vector2(0,0) )
	b_norms.push_back( up )
	b_verts.push_back( end + right * leave_link * -0.5 )
	
	b_uvs.push_back( Vector2(0,0) )
	b_norms.push_back( up )
	b_verts.push_back( end + right * leave_link * -0.5 )
	###### V
	b_uvs.push_back( Vector2(0,0) )
	b_norms.push_back( up )
	b_verts.push_back( end + right * leave_link * 0.5 )
	###### V
	b_uvs.push_back( Vector2(0,0) )
	b_norms.push_back( up )
	b_verts.push_back( start + right * leave_link * 0.5 )
	
	b_uvs.push_back( Vector2(0,0) )
	b_norms.push_back( right )
	b_verts.push_back( start + up * leave_link * 0.5 )
	###### V
	b_uvs.push_back( Vector2(0,0) )
	b_norms.push_back( right )
	b_verts.push_back( start + up * leave_link * -0.5 )
	###### V
	b_uvs.push_back( Vector2(0,0) )
	b_norms.push_back( right )
	b_verts.push_back( end + up * leave_link * -0.5 )
	
	b_uvs.push_back( Vector2(0,0) )
	b_norms.push_back( right )
	b_verts.push_back( end + up * leave_link * -0.5 )
	###### V
	b_uvs.push_back( Vector2(0,0) )
	b_norms.push_back( right )
	b_verts.push_back( end + up * leave_link * 0.5 )
	###### V
	b_uvs.push_back( Vector2(0,0) )
	b_norms.push_back( right )
	b_verts.push_back( start + up * leave_link * 0.5 )

func build_leaf(offset:Vector3, b:Basis, sca:float, branch:Dictionary):
	
	l_norms.push_back( branch.basis.y )
	l_tangents.push_back( branch.basis.x.x )
	l_tangents.push_back( branch.basis.x.y )
	l_tangents.push_back( branch.basis.x.z )
	l_tangents.push_back( 1 )
	l_uvs.push_back( Vector2(1,0) )
	l_verts.push_back( offset + b.xform( Vector3(sca*0.5,0,0) ) )
	###### V
	l_norms.push_back( branch.basis.y )
	l_tangents.push_back( branch.basis.x.x )
	l_tangents.push_back( branch.basis.x.y )
	l_tangents.push_back( branch.basis.x.z )
	l_tangents.push_back( 1 )
	l_uvs.push_back( Vector2(1,1) )
	l_verts.push_back( offset + b.xform( Vector3(sca*0.5,0,-sca) ) )
	###### V
	l_norms.push_back( branch.basis.y )
	l_tangents.push_back( branch.basis.x.x )
	l_tangents.push_back( branch.basis.x.y )
	l_tangents.push_back( branch.basis.x.z )
	l_tangents.push_back( 1 )
	l_uvs.push_back( Vector2(0,1) )
	l_verts.push_back( offset + b.xform( Vector3(-sca*0.5,0,-sca) ) )
	
	l_norms.push_back( branch.basis.y )
	l_tangents.push_back( branch.basis.x.x )
	l_tangents.push_back( branch.basis.x.y )
	l_tangents.push_back( branch.basis.x.z )
	l_tangents.push_back( 1 )
	l_uvs.push_back( Vector2(0,1) )
	l_verts.push_back( offset + b.xform( Vector3(-sca*0.5,0,-sca) ) )
	###### V
	l_norms.push_back( branch.basis.y )
	l_tangents.push_back( branch.basis.x.x )
	l_tangents.push_back( branch.basis.x.y )
	l_tangents.push_back( branch.basis.x.z )
	l_tangents.push_back( 1 )
	l_uvs.push_back( Vector2(0,0) )
	l_verts.push_back( offset + b.xform( Vector3(-sca*0.5,0,0) ) )
	###### V
	l_norms.push_back( branch.basis.y )
	l_tangents.push_back( branch.basis.x.x )
	l_tangents.push_back( branch.basis.x.y )
	l_tangents.push_back( branch.basis.x.z )
	l_tangents.push_back( 1 )
	l_uvs.push_back( Vector2(1,0) )
	l_verts.push_back( offset + b.xform( Vector3(sca*0.5,0,0) ) )

func build_mesh():
	
	if fast_imm != null:
		fast_imm.clear()
	
	# trunk array
	t_verts = PoolVector3Array()
	t_norms = PoolVector3Array()
	t_tangents = PoolRealArray()
	t_uvs = PoolVector2Array()
	
#	var _mesh = Mesh.new()
#	var _surf = SurfaceTool.new()
#	_surf.begin(Mesh.PRIMITIVE_TRIANGLES)
	
	var agap = TAU / trunk_definition
	for branch in branches:
		
		# construction of top vertices
		var Y = branch.basis.y
		var Z = null
		if branch.parent == null:
			Z = Y.cross(Vector3.RIGHT).normalized()
		else:
			Z = Y.cross(branch.parent.basis.x).normalized()
		var X = Z.cross(Y).normalized()
		
		var plevel = 0
		var build_bottom = true
		var bottom_vertices = []
		var bottom_normals = []
		var bottom_tangents = []
		branch.vertices = []
		branch.normals = []
		branch.tangents = []
		
		if branch.parent != null:
			plevel = branch.parent.level
			build_bottom = false
			bottom_vertices = branch.parent.vertices
			bottom_normals  = branch.parent.normals
			bottom_tangents  = branch.parent.tangents
		
		var bot_tk:float = thickness_bottom + (thickness_top-thickness_bottom) * plevel / depth
		var top_tk:float = thickness_bottom + (thickness_top-thickness_bottom) * branch.level / depth
		branch.thickness = top_tk
		
		for i in range(0,trunk_definition):
			var a:float = agap * i
			var c:float = cos( a )
			var s:float = sin( a )
			if build_bottom:
				bottom_vertices.append( branch.bottom + c * bot_tk * X + s * bot_tk * Z )
				bottom_normals.append( (c * X + s * Z).normalized() )
				bottom_tangents.append( Plane((s * X + c * Z).normalized(),0) )
			branch.vertices.append( branch.top + c * top_tk * X + s * top_tk * Z )
			branch.normals.append( (c * X + s * Z).normalized() )
			branch.tangents.append( Plane((s * X + c * Z).normalized(),0) )
		
		var bh = max_length - (branch.total_length - branch.length)
		var th = max_length - branch.total_length
		
		var ugap = 1.0 / trunk_definition
		for i in range(0,trunk_definition):
			var h:int = (i+(trunk_definition-1)) % trunk_definition
			var j:int = (i+1) % trunk_definition
			build_branch( 
				ugap, i, j, 
				branch, 
				{
					'vertices':bottom_vertices,
					'normals':bottom_normals,
					'tangents':bottom_tangents
				} )
	
	var trunk = []
	trunk.resize(ArrayMesh.ARRAY_MAX)
	trunk[ArrayMesh.ARRAY_VERTEX] = t_verts
	trunk[ArrayMesh.ARRAY_NORMAL] = t_norms
	trunk[ArrayMesh.ARRAY_TANGENT] = t_tangents
	trunk[ArrayMesh.ARRAY_TEX_UV] = t_uvs
	
	var leaves_arr = []
	var leafb_arr = []
	
	if leaves:
	
		# leaves array
		l_verts = PoolVector3Array()
		l_norms = PoolVector3Array()
		l_tangents = PoolRealArray()
		l_uvs = PoolVector2Array()
		
		if leave_link != 0:
			b_verts = PoolVector3Array()
			b_norms = PoolVector3Array()
			b_tangents = PoolRealArray()
			b_uvs = PoolVector2Array()
		
		for branch in branches:
			if branch.level < leave_start:
				continue
			agap = TAU / 6
			var top_tk = branch.thickness
			var bot_tk = branch.thickness
			if branch.parent != null:
				 bot_tk = branch.parent.thickness
			for i in range(0,6):
				var a = agap * i
				var sca = leave_scale
				var hoff = rand_range( -0.1, 0.3 )
				var thick = max(0,bot_tk + (top_tk-bot_tk) * hoff)
				var fwd = ( cos(a) * branch.basis.x + sin(a) * branch.basis.z )
				var offset = branch.top + fwd * (thick+leave_offset)
				var smult = max(0,min(1,leave_up + rand_range(-0.1,0.1)))
				var planey = branch.basis.y.slerp( Vector3.UP, smult ).normalized()
				randomize()
				planey += branch.basis.x*rand_range(-leave_up_noise,leave_up_noise)
				planey += branch.basis.z*rand_range(-leave_up_noise,leave_up_noise)
				planey = planey.normalized()
				var planez = planey.cross( branch.basis.x ).normalized()
				var planex = planez.cross( planey ).normalized()
				var plane = Basis(planex,planey,planez)
				var lup = branch.basis.y * branch.length * hoff
				offset -= lup
				var b = plane*Basis(Vector3.UP,TAU*3/4-a)
				build_leaf( offset, b, sca, branch )
				if leave_link != 0:
					if hoff < 0:
						lup = Vector3.ZERO
					build_tail( branch.top - lup, offset )
		
		leaves_arr.resize(ArrayMesh.ARRAY_MAX)
		leaves_arr[ArrayMesh.ARRAY_VERTEX] = l_verts
		leaves_arr[ArrayMesh.ARRAY_NORMAL] = l_norms
		leaves_arr[ArrayMesh.ARRAY_TANGENT] = l_tangents
		leaves_arr[ArrayMesh.ARRAY_TEX_UV] = l_uvs
		if leave_link != 0:
			leafb_arr.resize(ArrayMesh.ARRAY_MAX)
			leafb_arr[ArrayMesh.ARRAY_VERTEX] = b_verts
			leafb_arr[ArrayMesh.ARRAY_NORMAL] = b_norms
#			leafb_arr[ArrayMesh.ARRAY_TANGENT] = b_tangents
			leafb_arr[ArrayMesh.ARRAY_TEX_UV] = b_uvs
	
	# Create the Mesh.
	var arr_mesh = ArrayMesh.new()
	arr_mesh.add_surface_from_arrays(Mesh.PRIMITIVE_TRIANGLES, trunk)
	if leaves:
		arr_mesh.add_surface_from_arrays(Mesh.PRIMITIVE_TRIANGLES, leaves_arr)
	if leaves and leave_link != 0:
		arr_mesh.add_surface_from_arrays(Mesh.PRIMITIVE_TRIANGLES, leafb_arr)
		
	mesh = arr_mesh
	mesh.surface_set_material( 0, trunk_mat )
	if leaves:
		mesh.surface_set_material( 1, leaves_mat )
	if leaves and leave_link != 0:
		mesh.surface_set_material( 2, leaflinks_mat )
	
	# freeing memory
	t_verts = PoolVector3Array()
	t_norms = PoolVector3Array()
	t_tangents = PoolRealArray()
	t_uvs = PoolVector2Array()
	l_verts = PoolVector3Array()
	l_norms = PoolVector3Array()
	l_tangents = PoolRealArray()
	l_uvs = PoolVector2Array()
	b_verts = PoolVector3Array()
	b_norms = PoolVector3Array()
	b_tangents = PoolRealArray()
	b_uvs = PoolVector2Array()

func wireframe_init():
	if fast_mat == null:
		fast_mat = SpatialMaterial.new()
		fast_mat.flags_unshaded = true
		fast_mat.vertex_color_use_as_albedo = true	
	if fast_imm == null:
		fast_imm = ImmediateGeometry.new()
		add_child( fast_imm )
		fast_imm.material_override = fast_mat

func wireframe():
	mesh = null
	if fast_imm == null:
		wireframe_init()
	fast_imm.clear()
	fast_imm.begin(Mesh.PRIMITIVE_LINES)
	for b in branches:
		var bl = (b.total_length-b.length)/max_length
		var tl = b.total_length/max_length
		var bc = Vector3(1,0,1) * (1-bl) + Vector3(0,1,1) * bl
		var tc = Vector3(1,0,1) * (1-tl) + Vector3(0,1,1) * tl
		fast_imm.set_color( Color(bc.x,bc.y,bc.z) )
		fast_imm.add_vertex( b.bottom )
		fast_imm.set_color( Color(tc.x,tc.y,tc.z) )
		fast_imm.add_vertex( b.top )
	fast_imm.end()

func build():
	
	mesh = null
	if fast_imm != null:
		fast_imm.clear()
	
	build_hierarchy()
	build_splits()
	build_length()
	build_positions()

	if fast:
		wireframe()
	else:
		build_mesh()

func _enter_tree():
	if length == null:
		length = Curve.new()
		length.min_value = 0
		length.max_value = 5
		length.add_point(Vector2(0,3))
		length.add_point(Vector2(1,1))
	if split_count == null:
		split_count = Curve.new()
		split_count.min_value = 0
		split_count.max_value = 5
		split_count.add_point(Vector2(0,2))
		split_count.add_point(Vector2(1,4))
