#tool

extends Node

export (Array,NodePath) var wrong_pads:Array = []
export (float) var wrong_radius:float = 2.5
export (Array,NodePath) var good_pads:Array = []
export (float) var good_radius:float = 0.5
export (NodePath) var planet:NodePath = ""
export (Vector3) var planet_rot:Vector3 = Vector3(1,0,0)
export (NodePath) var omni:NodePath = ""
export (float) var omni_min_energy:float = 0.5
export (float) var omni_max_energy:float = 1.5
export (NodePath) var environment:NodePath = ""
export (Color) var horizon_on:Color = Color(1,1,1,1)

onready var gate = get_parent()
onready var light:Light = get_node(omni)
onready var env:Environment = get_node(environment).environment
var wpad:Array = []
var gpad:Array = []
var pl:Spatial = null

var light_energy_target:float = 0
var sun_energy_target:float = 0
var planet_rot_target:float = 0
var planet_rot_curr:float = 0

func _ready():
	for np in wrong_pads:
		var wp = get_node(np)
		wp.get_child(0).light_energy = omni_min_energy
		wpad.append( wp )
	for np in good_pads:
		gpad.append( get_node(np) )
	light.light_energy = 0
	env.background_sky.sun_energy = 0
	env.background_sky.sky_horizon_color = horizon_on
	env.background_sky.ground_horizon_color = horizon_on
	
	light_energy_target = 0

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	
	if pl == null and planet != "":
		pl = get_node( planet )
	
	light_energy_target = 0
	sun_energy_target = 0
	planet_rot_target = 0
	
	# compute distances to pads
	var vp:Vector3 = gate.player.global_transform.origin
	for wp in wpad:
		var diff:Vector3 = ( vp - wp.global_transform.origin ) * Vector3(1,0,1)
		var l = diff.length()
		if l <= wrong_radius:
			var pc = 1 - (l/wrong_radius)
			pc = max(1,pc*3)
			wp.get_child(0).light_energy = omni_min_energy + (omni_max_energy-omni_min_energy) * pc
			light_energy_target = 0.2
		elif wp.get_child(0).light_energy > omni_min_energy:
			wp.get_child(0).light_energy += (omni_min_energy-wp.get_child(0).light_energy)*5*delta
	
	for gp in gpad:
		var diff:Vector3 = ( vp - gp.global_transform.origin ) * Vector3(1,0,1)
		var l = diff.length()
		if l <= good_radius:
			planet_rot_target = 1
			light_energy_target = 1
			sun_energy_target = 2
	
	if light.light_energy != light_energy_target:
		light.light_energy += (light_energy_target-light.light_energy) * 2 * delta
		
	if env.background_sky.sun_energy != sun_energy_target:
		env.background_sky.sun_energy += (sun_energy_target-env.background_sky.sun_energy) * 5 * delta
		env.background_sky.ground_energy = env.background_sky.sun_energy * 0.5
		env.background_sky.sky_energy = env.background_sky.sun_energy * 0.5
		
	if planet_rot_curr != planet_rot_target:
		planet_rot_curr += (planet_rot_target-planet_rot_curr) * 5 * delta
	if pl != null:
		pl.rotation += planet_rot * planet_rot_curr * delta
	
