extends Node

export (String) var ip:String = "127.0.0.1"
export (int) var port:int = 25000

onready var player:Spatial = get_parent().get_player()
var sender = null
var pid:int = 0
var path:String = ""

func _ready():
	sender = load("res://addons/gdosc/gdoscsender.gdns").new()
	sender.setup( ip, port )
	sender.start()

func _process(delta):
	
	sender.msg("/player")
	sender.add( player.global_transform.origin )
	sender.add( player.global_transform.basis.get_euler() )
	sender.send()
	
	if path != "":
		sender.msg("/scene")
		sender.add( pid )
		sender.add( path )
		sender.send()
		path = ""

func _exit_tree():
	sender.stop()
