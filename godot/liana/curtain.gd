tool

extends ImmediateGeometry

export (Array,NodePath) var lianas:Array = [] setget set_lianas

func set_lianas( ar:Array ):
	lianas = ar
	initialised = false

var initialised:bool = false
var ropes:Array = []
var definition:int = 0

func _process(delta):
	
	if !initialised:
		
		initialised = true
		definition = 0
		ropes = []
		
		for l in lianas:
			var liana = get_node( l )
			if liana == null:
				ropes = []
				break
			if definition == 0:
				definition = liana.splits
			elif definition != liana.splits:
				ropes = []
				break
			ropes.append( liana )
	
	clear()
	begin( Mesh.PRIMITIVE_TRIANGLES )
	
	var rnum = ropes.size()
	var uvnum = ropes.size() - 1
	for i in range(1,rnum):
		
		var top = ropes[i-1]
		var bottom = ropes[i]
		
		for j in range(0,definition):
			
			var v0:Vector3 = top.segments[j].pos
			var v1:Vector3 = top.segments[j+1].pos
			var v2:Vector3 = bottom.segments[j+1].pos
			var v3:Vector3 = bottom.segments[j].pos
			
			var norm:Vector3 = (v1-v0).cross(v1-v2).normalized()
			
			set_uv( Vector2((j*1.0)/definition, (i-1.0)/uvnum) )
			set_normal( norm )
			add_vertex( v0 )
			###### V
			set_uv( Vector2((j+1.0)/definition, (i-1.0)/uvnum) )
			set_normal( norm )
			add_vertex( v1 )
			###### V
			set_uv( Vector2((j+1.0)/definition, (i*1.0)/uvnum) )
			set_normal( norm )
			add_vertex( v2 )
			
			set_uv( Vector2((j+1.0)/definition, (i*1.0)/uvnum) )
			set_normal( norm )
			add_vertex( v2 )
#			###### V
			set_uv( Vector2((j*1.0)/definition, (i*1.0)/uvnum) )
			set_normal( norm )
			add_vertex( v3 )
#			###### V
			set_uv( Vector2((j*1.0)/definition, (i-1.0)/uvnum) )
			set_normal( norm )
			add_vertex( v0 )
			
	end()
		
