extends Spatial

export (bool) var play = true
export (bool) var root_translation = false
export (float,1,100) var ccdik_iterations = 30
export (float,1,10) var ccdik_tolerance = 5
export (float,0,100) var ccdik_interpolation = 10
export (bool) var debug = false
export (bool) var hide_trackers = true

var tracker_debug_mat:SpatialMaterial = null

func tracker_debugger():
	var im:ImmediateGeometry = ImmediateGeometry.new()
	im.add_sphere( 24,24,1,false )
	im.begin( Mesh.PRIMITIVE_LINES )
	im.add_vertex( Vector3(1,0,0) )
	im.add_vertex( Vector3(0,1,0) )
	im.add_vertex( Vector3(1,0,0) )
	im.add_vertex( Vector3(0,-1,0) )
	im.add_vertex( Vector3(1,0,0) )
	im.add_vertex( Vector3(0,0,1) )
	im.add_vertex( Vector3(1,0,0) )
	im.add_vertex( Vector3(0,0,-1) )
	im.add_vertex( Vector3(0,1,0) )
	im.add_vertex( Vector3(0,0,1) )
	im.add_vertex( Vector3(0,1,0) )
	im.add_vertex( Vector3(0,0,-1) )
	im.add_vertex( Vector3(-1,0,0) )
	im.add_vertex( Vector3(0,1,0) )
	im.add_vertex( Vector3(-1,0,0) )
	im.add_vertex( Vector3(0,-1,0) )
	im.add_vertex( Vector3(-1,0,0) )
	im.add_vertex( Vector3(0,0,1) )
	im.add_vertex( Vector3(-1,0,0) )
	im.add_vertex( Vector3(0,0,-1) )
	im.add_vertex( Vector3(0,-1,0) )
	im.add_vertex( Vector3(0,0,-1) )
	im.add_vertex( Vector3(0,-1,0) )
	im.add_vertex( Vector3(0,0,1) )
	im.end()
	if tracker_debug_mat == null:
		tracker_debug_mat = SpatialMaterial.new()
		tracker_debug_mat.flags_unshaded = true
		tracker_debug_mat.flags_no_depth_test = true
		tracker_debug_mat.albedo_color = Color(0,1,1,1)
	im.material_override = tracker_debug_mat
	im.scale *= 0.3
	return im

func _ready():
	
	$ccdik.play(play)
	$ccdik._verbose = false
	$ccdik._debug_iterations = debug
	$ccdik._root_translation = root_translation
	$ccdik._iterations = ccdik_iterations
	$ccdik._tolerance = ccdik_tolerance
	$ccdik._interpolation_speed = ccdik_interpolation
	
	$trackers.visible = !hide_trackers
	$trackers/index.visible = !hide_trackers
	$trackers/middle.visible = !hide_trackers
	$trackers/pinky.visible = !hide_trackers
	$trackers/ring.visible = !hide_trackers
	$trackers/thumb.visible = !hide_trackers
	
	if debug and !hide_trackers:
		$trackers.add_child( tracker_debugger() )
		$trackers/thumb.add_child( tracker_debugger() )
		$trackers/index.add_child( tracker_debugger() )
		$trackers/middle.add_child( tracker_debugger() )
		$trackers/ring.add_child( tracker_debugger() )
		$trackers/pinky.add_child( tracker_debugger() )

func get_trackers():
	return [
		$trackers,
		$trackers/thumb,
		$trackers/index,
		$trackers/middle,
		$trackers/ring,
		$trackers/pinky
	]
