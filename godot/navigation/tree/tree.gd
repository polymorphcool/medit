tool

extends Node2D

export (NodePath) var entry:NodePath = ""

func get_target():
	return get_node( entry )

func set_player( i:int, node:Sprite ):
	if i == 0:
		$p0.position = node.position
	elif i == 1:
		$p1.position = node.position
