extends Node

export(Array,VideoStream) var videos
export(bool) var loop : bool = true
export(bool) var mute : bool = false
export(float) var transition : float = 0
export(NodePath) var viewport = ""
export(NodePath) var player = ""
export(NodePath) var surface = ""
export(bool) var verbose : bool = false

var streams : Array
var active : bool = false
var video_binded : bool = false
var vp : Viewport = null
var vplayer : VideoPlayer = null
var mesh : MeshInstance = null
var mat : Material = null
var fallback_texture : Texture = null
var curr_stream : int = -1
var fade_active : bool = false
var fade_out_time : float = 0
var fade_in_time : float = 0
var fast_forward : int = 0

func _ready():
	for v in videos:
		if v != null:
			streams.append(v)
	if streams.empty():
		if verbose:
			print( "stream_manager: NO STREAM TO MANAGE" )
		return
	if viewport == "" or get_node(viewport) == null or !get_node(viewport) is Viewport:
		if verbose:
			print( "stream_manager: NO VIEWPORT" )
		return
	if player == "" or get_node(player) == null or !get_node(player) is VideoPlayer:
		if verbose:
			print( "stream_manager: NO VIDEO PLAYER" )
		return
	if surface == "" or get_node(surface) == null or !get_node(surface) is MeshInstance:
		if verbose:
			print( "stream_manager: NO SURFACE" )
		return
	vp = get_node(viewport)
	vplayer = get_node(player)
	vplayer.connect( "finished", self, "video_finished" )
	mesh = get_node(surface)
	mat = mesh.material_override
	if verbose:
		print( "stream_manager: started successfully" )
	active = true

# warning-ignore:unused_argument
func _process(delta):
	if !active:
		return
	if fade_out_time > 0:
		fade_out_time -= delta
		if fade_out_time <= 0:
			set_fade(1)
			fade_out_end()
		else:
			set_fade(1-(fade_out_time*2/transition))
	elif fade_in_time > 0:
		fade_in_time -= delta
		if fade_in_time <= 0:
			set_fade(0)
			fade_in_end()
		else:
			set_fade(fade_in_time*2/transition)

func set_fade( pc : float ):
	var w : float = (1+sin(-PI*0.5+PI*pc))*0.5
	var c : Color = Color( 1-w, 1-w, 1-w, 1 )
	if mat is SpatialMaterial:
		mat.albedo_color = c
	elif mat is ShaderMaterial:
		mat.set_shader_param( "albedo", c )

func fade_out_end():
	
	if !video_binded:
		bind_video()
	
	if curr_stream < 0:
		curr_stream = 0
	else:
		curr_stream += 1
	
	curr_stream += fast_forward
	fast_forward = 0
	curr_stream %= streams.size()
	
	vplayer.stream = streams[curr_stream]
	vplayer.play()
	# adjust viewport to video texture size
	vp.size = vplayer.get_video_texture().get_size()

func fade_in_end():
	fade_active = false
	if fast_forward > 0:
		fast_forward -= 1
		next_video()

func next_video():
	
	# avoid glitch in case of multiple next request
	if fade_active:
		fast_forward += 1
		return
	
	if transition <= 0:
		set_fade(0)
		fade_out_end()
	else:
		fade_out_time = transition / 2
		fade_in_time = transition / 2
		fade_active = true
		# video switch will be managed by fade timers

func bind_video():
	
	var t : ViewportTexture = vp.get_texture()
	t.flags = Texture.FLAG_FILTER | Texture.FLAG_VIDEO_SURFACE
	
	if mat is SpatialMaterial:
		fallback_texture = mat.albedo_texture
		mat.albedo_texture = t
	elif mat is ShaderMaterial:
		fallback_texture = mat.get_shader_param( "texture_albedo" )
		mat.set_shader_param( "texture_albedo", t )
		mat.set_shader_param( "srgb_linear", 1 )
	video_binded = true

func video_finished():
	if !loop:
		return
	else:
		vplayer.play()

func _input(event):
	
	if !active:
		return
	
	if event is InputEventKey and event.pressed and event.scancode == KEY_SPACE:
		
		next_video()
		
		if verbose:
			print( "starting stream: ", streams[curr_stream].get_file(), ", size: ", vp.size )
