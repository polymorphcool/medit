tool

extends MeshInstance

export (Vector3) var planet_rot:Vector3 = Vector3(1,0,0)

func _process(delta):
	rotation += planet_rot * delta
