tool

extends Node2D

export (NodePath) var p0:NodePath = "" setget set_p0
export (NodePath) var p1:NodePath = "" setget set_p1
export (String) var scene_folder:String = "res://navigation/scenes/"
export (String) var scene:String = "" setget set_scene

func set_p0( np:NodePath ):
	p0 = np
	t0 = get_node(p0)
	if t0 == null and l0 != null:
		remove_child( l0 )
		l0 = null
	elif t0 != null and l0 == null:
		l0 = Line2D.new()
		add_child( l0 )
		l0.width = 2
		l0.default_color = Color(1,0,1)
		l0.add_point(Vector2.ZERO)
		l0.add_point(Vector2.ZERO)
		l0.add_point(Vector2.ZERO)

func set_p1( np:NodePath ):
	p1 = np
	t1 = get_node(p1)
	if t1 == null and l1 != null:
		remove_child( l1 )
		l1 = null
	elif t1 != null and l1 == null:
		l1 = Line2D.new()
		add_child( l1 )
		l1.width = 2
		l1.default_color = Color(0,1,1)
		l1.add_point(Vector2.ZERO)
		l1.add_point(Vector2.ZERO)
		l1.add_point(Vector2.ZERO)

func set_scene( s:String ):
	scene = s
	if lbl == null:
		for c in get_children():
			if c is Label:
				lbl = c
				break
		if lbl == null:
			lbl = Label.new()
			add_child( lbl )
			lbl.rect_position = Vector2(0,10)
	lbl.text = scene
	lbl.rect_position.x = -lbl.rect_size.x*0.5

func get_target(i:int):
	if i == 0:
		return t0
	elif i == 1:
		return t1
	return null

var lbl:Label = null
var t0:Sprite = null
var l0:Line2D = null
var t1:Sprite = null
var l1:Line2D = null
var initialised:bool = false

func preload_targets():
	if t0 != null and t0.scene != "":
		load( t0.scene_folder + t0.scene )
	if t1 != null and t1.scene != "":
		load( t1.scene_folder + t1.scene )

func reset():
	while get_child_count() > 0:
		remove_child( get_child(0) )
	lbl = null
	t0 = null
	l0 = null
	t1 = null
	l1 = null

func _process(delta):
#	if !Engine.editor_hint:
#		return
	if !initialised:
		initialised = true
		set_p0(p0)
		set_p1(p1)
		set_scene( scene )
		set_scene( scene )
	
	if l0 != null:
		var diff = t0.position-position
		var perp = diff.normalized().rotated( PI*-0.5 )
		l0.set_point_position( 1, diff*0.5 + perp * diff.length()*0.1 )
		l0.set_point_position( 2, diff )
	if l1 != null:
		var diff = t1.position-position
		var perp = diff.normalized().rotated( PI*0.5 )
		l1.set_point_position( 1, diff*0.5 + perp * diff.length()*0.1 )
		l1.set_point_position( 2, diff )
