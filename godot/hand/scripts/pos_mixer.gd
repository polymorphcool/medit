tool

extends Node

export (bool) var play:bool = false setget set_play
export (NodePath) var root_path:NodePath = "" setget set_root_path
export (String) var export_folder:String = "res://hand/poses/" setget set_export_folder
export (Array) var poses:Array = []

var root_node:Spatial = null
var trackers:Dictionary = {}

func set_play( b:bool ):
	play = b
	if play:
		trackers = {}
		refresh_poses()

func set_root_path( np:NodePath ):
	root_path = np
	root_node = get_node(np)
	if root_node != null and not root_node is Spatial:
		root_node = null

func set_export_folder( s:String ):
	export_folder = s
	refresh_poses()

func parse_pose( path:String ):
	var jf = File.new()
	jf.open( path, File.READ)
	var txt = parse_json(jf.get_as_text())
	var pose = []
	for tp in txt:
		tp.pos = Vector3( tp.pos[0], tp.pos[1], tp.pos[2] )
		var euler = Vector3(tp.rot[0], tp.rot[1], tp.rot[2] )
		tp.rot = Quat(Vector3.UP,tp.rot[1])*Quat(Vector3.RIGHT,tp.rot[0])*Quat(Vector3.BACK,tp.rot[2])
		tp.sca = Vector3(tp.sca[0], tp.sca[1], tp.sca[2] )
		pose.append( tp )
	jf.close()
	return pose

func refresh_poses():
	
	var files = []
	var dir = Directory.new()
	dir.open(export_folder)
	dir.list_dir_begin()
	while true:
		var file = dir.get_next()
		if file == "":
			break
		elif file.ends_with(".json"):
			files.append(file)
	dir.list_dir_end()
	files.sort()
	
	poses = []
	for f in files:
		poses.append({
				'name': f.replace(".json", ""),
				'weight': float(0),
				'pose': parse_pose( export_folder + f )
			}
		)
	load_trackers()

func _ready():
	set_root_path( root_path )
	refresh_poses()

func load_trackers():
	
	if root_node == null:
		trackers = {}
		return
	for p in poses:
		for el in p.pose:
			if not el.node in trackers:
				var node:Spatial = root_node.get_node( el.node )
				if node == null or not node is Spatial:
					printerr( "Error while loading tracker: ", el.node )
					trackers = {}
					return
				var q = Quat()
				q.set_euler( node.rotation )
				trackers[el.node] = {
					'node': node,
					'init': {
						'pos': node.translation,
						'rot': q,
						'sca': node.scale
					},
					'pose': {
						'pos': node.translation,
						'rot': q,
						'sca': node.scale
					}
				}

func apply_poses():
	
	if trackers.empty():
		load_trackers()
	
	# reseting trackers
	for t in trackers.keys():
		var init = trackers[t].init
		var pose = trackers[t].pose
		pose.pos = init.pos
		pose.rot = init.rot
		pose.sca = init.sca
	
	for p in poses:
		if p.weight != 0:
			var w = p.weight
			var wi = max(0,1-abs(w))
			for el in p.pose:
				if not el.node in trackers:
					continue
				var t = trackers[el.node]
				t.pose.pos = t.pose.pos * wi +  el.pos * w
				t.pose.rot = t.pose.rot.slerp( el.rot, w )
				t.pose.sca = t.pose.sca * wi +  el.sca * w
	
	# applying poses
	for t in trackers.keys():
		var node = trackers[t].node
		var pose = trackers[t].pose
		node.translation = 	pose.pos
		node.rotation = 	pose.rot.get_euler()
		node.scale = 		pose.sca

func _process(delta):
	if not play or root_node == null or !root_node.visible:
		return
	apply_poses()
