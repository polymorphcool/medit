extends "handfx_common.gd"

export (NodePath) var target:NodePath = ""
export (float,0,1) var touching_offset:float = 0.3

var touch_node:Spatial = null
var rays:Array = []
var offsets:Array = []

func _ready():
	fx_initialise()
	touch_node = get_node(target)
	if touch_node != null and not touch_node is RigidBody:
		printerr( "target ", target, " MUST be a RigidBody" )
		touch_node = null
	for i in range(0,len(trackers)):
		var t = trackers[i]
		offsets.append( transforms[i].origin-transforms[2].origin)
		if mask[i]:
			var r = RayCast.new()
			add_child( r )
			r.collide_with_bodies = true
			rays.append(r)
		else:
			rays.append(null)

# warning-ignore:unused_argument
func _process(delta):
	if !fx_ready():
		return
	if touch_node == null:
		return
	
	var tb = touch_node.global_transform.basis
	var to = touch_node.global_transform.origin
	var pr = null
	
	for i in range(0,len(trackers)):
		var trck = trackers[i]
		if i == 0:
			pr = trck.global_transform.looking_at(to,tb.y)
			continue
		if !mask[i]:
			continue
		var tran = transforms[i]
		var ray = rays[i]
		var offset = pr.xform(offsets[i]) - pr.origin
		var o = pr.xform(tran.origin)
		ray.global_transform.origin = o
		ray.cast_to = to + offset * touching_offset - o
		ray.force_raycast_update()
		if ray.is_colliding():
			trck.global_transform.origin = ray.get_collision_point()
