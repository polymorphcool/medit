extends Spatial

export (int,0,100) var grid:int = 5
export (int,0,100) var radius:int = 5
export (float,0,10) var cell:float = 1
export (float,0,10) var pastille:float = 0.5
export (float,0,10) var player_radius:float = 2
export (float,-10,10) var player_push:float = 1
export (bool) var generate:bool = true

var player_pos = Vector3.ZERO

func _ready():
	get_child(0).visible = false

func _process(delta):
	
	var tmpl = get_child(0)
	
	if generate:
		generate = false
		while get_child_count() > 1:
			remove_child( get_child(1) )
		for z in range( -grid, grid+1 ):
			for x in range( -grid, grid+1 ):
				var pos = Vector3(x*cell,0,z*cell)
				if pos.length() > radius:
					continue
				var p = tmpl.duplicate()
				add_child( p )
				p.name = "pastille"
				p.visible = true
				p.translation.x = pos.x
				p.translation.z = pos.z
				p.scale = Vector3(pastille,1,pastille)
	
	for i in range(1,get_child_count()):
		var c:Spatial = get_child(i)
		var d = (player_pos - c.translation) * Vector3(1,0,1)
		var l = d.length()
		if l <= player_radius:
			var pc = 1 - (l/player_radius)
			c.translation.y = tmpl.translation.y + (1+sin(-PI*0.5+PI*pc)) * -0.5 * player_push
	
