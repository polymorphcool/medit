tool

extends ImmediateGeometry

export (bool) var generate:bool = false setget set_generate
export (bool) var wireframe:bool = false
export (bool) var leaves:bool = false
export (int,0,10) var depth:int = 5
export (int,0,10) var split:int = 3
export (int,2,24) var definition:int = 6
export (float,0,1) var deviation:float = 0.5
export (float,0,5) var length_bottom:float = 0.7
export (float,0,5) var length_top:float = 0.9
export (float,0,1) var length_deviation:float = 0.5
export (float,0,1) var bottom_thickness:float = 0.3
export (float,0,1) var top_thickness:float = 0.1
export (int,0,10) var leave_start:int = 2
export (float,0,1) var leave_scale:float = 0.1
export (float,0,1) var leave_up:float = 0.8

var branches:Array = []

func set_generate( b:bool ):
	generate = false
	if b:
		build_tree()

func new_branch( pos:Vector3, dir:Vector3, length:float, basis:Basis, level:int ):
	var b = {
		'pos': pos,
		'dir': dir,
		'top': pos + dir * length,
		'length': length,
		'basis': basis,
		'level': level,
		'bottom_verts': null
	}
	branches.append(b)
	return b

func add_volume( branch:Dictionary, bottom_thick:float, top_thick:float, bot_verts = null ):
#	if wireframe:
#		add_vertex( branch.pos )
#		add_vertex( branch.pos + branch.dir * branch.length )
#		return null
#	else:
	var X = branch.basis.x
	var Y = branch.basis.y
	var Z = branch.basis.z
	var bottom = branch.pos
	var top = branch.top
	var build_bottom:bool = bot_verts == null
	if build_bottom:
		bot_verts = []
	var top_verts:Array = []
	var normals:Array = []
	var agap = TAU / definition
	for i in range(0,definition):
		var a:float = agap * i
		var c:float = cos( a )
		var s:float = sin( a )
		if build_bottom:
			bot_verts.append( bottom + c * bottom_thick * X + s * bottom_thick * Z )
		top_verts.append( top + c * top_thick * X + s * top_thick * Z )
		normals.append( (c * X + s * Z).normalized() )
	for i in range(0,definition):
		var h:int = (i+(definition-1)) % definition
		var j:int = (i+1) % definition
		var n:Vector3 = (normals[i]+normals[j]).normalized()
		set_normal( normals[i] )
		set_uv( Vector2(0,0) )
		add_vertex( bot_verts[i] )
		set_normal( normals[i] )
		set_uv( Vector2(0,0) )
		add_vertex( top_verts[i] )
		set_normal( normals[j] )
		set_uv( Vector2(0,0) )
		add_vertex( top_verts[j] )
		set_normal( normals[j] )
		set_uv( Vector2(0,0) )
		add_vertex( top_verts[j] )
		set_normal( normals[j] )
		set_uv( Vector2(0,0) )
		add_vertex( bot_verts[j] )
		set_normal( normals[i] )
		set_uv( Vector2(0,0) )
		add_vertex( bot_verts[i] )
	return top_verts

func add_branches( origins:Array, level:int ):
	var out:Array = []
	var bottom_thickn:float = bottom_thickness + (top_thickness-bottom_thickness) * (level-1) / depth
	var top_thickn:float = bottom_thickness + (top_thickness-bottom_thickness) * level / depth
	var l = length_bottom + (length_top-length_bottom) * level / depth 
	for o in origins:
		for s in range(0,split):
			randomize()
			var p = o.pos + o.dir * o.length
			var dir = (o.dir + Vector3( rand_range(-deviation,deviation), rand_range(-deviation,deviation), rand_range(-deviation,deviation) )).normalized()
			var Y = dir.normalized()
			var Z = Y.cross( Vector3.RIGHT ).normalized()
			var X = Z.cross( Y ).normalized()
			var no = new_branch( 
				p, dir, l + rand_range(-length_deviation,length_deviation),
				Basis(X,Y,Z), level )
			no.bottom_verts = add_volume( no, bottom_thickn, top_thickn, o.bottom_verts )
			out.append( no )
	return out

func add_leaves( branch:Dictionary ):
	if branch.level < leave_start:
		return
	var agap:float = TAU / 6
	for i in range(0,6):
		var a = agap * i
		var sca = leave_scale * rand_range( 0.8, 1.2 )
		var offset = branch.top + ( cos(a) * branch.basis.x + sin(a) * branch.basis.z ) * sca * 2
		var smult = max(0,min(1,leave_up + rand_range(-0.1,0.1)))
		var planey = branch.basis.y.slerp( Vector3.UP, smult ).normalized()
		var planez = planey.cross( branch.basis.x ).normalized()
		var planex = planez.cross( planey ).normalized()
		var plane = Basis(planex,planey,planez)
		offset += plane.y * rand_range( -0.1, 0.1 )
		var b = plane*Basis(Vector3.UP,TAU*3/4-a)
		set_normal( branch.basis.y )
		set_uv( Vector2(1,0) )
		add_vertex( offset + b.xform( Vector3(sca,0,sca) ) )
		set_normal( branch.basis.y )
		set_uv( Vector2(1,1) )
		add_vertex( offset + b.xform( Vector3(sca,0,-sca) ) )
		set_normal( branch.basis.y )
		set_uv( Vector2(0,1) )
		add_vertex( offset + b.xform( Vector3(-sca,0,-sca) ) )
		set_normal( branch.basis.y )
		set_uv( Vector2(0,1) )
		add_vertex( offset + b.xform( Vector3(-sca,0,-sca) ) )
		set_normal( branch.basis.y )
		set_uv( Vector2(0,0) )
		add_vertex( offset + b.xform( Vector3(-sca,0,sca) ) )
		set_normal( branch.basis.y )
		set_uv( Vector2(1,0) )
		add_vertex( offset + b.xform( Vector3(sca,0,sca) ) )

func build_tree():
	branches = []
	clear()
	if wireframe:
		begin( Mesh.PRIMITIVE_LINES )
	else:
		begin( Mesh.PRIMITIVE_TRIANGLES )
	var o = new_branch( Vector3(0,0,0), Vector3.UP, length_bottom, Basis(Vector3.RIGHT,Vector3.UP,Vector3.FORWARD), 0 )
	o.bottom_verts = add_volume( o, bottom_thickness, bottom_thickness )
	var lvl_branches:Array = [o]
	for i in range(0,depth):
		lvl_branches = add_branches( lvl_branches, i+1 )
	if leaves:
		for b in branches:
			add_leaves( b )
	end()
