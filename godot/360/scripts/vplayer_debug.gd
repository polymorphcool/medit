extends VideoPlayer

var space_down : bool = true

func _ready():
	print( "VideoPlayer ready" )
	set_process_input(true)
	pass

func _process(delta):
	
	var prev : bool = space_down
	if Input.is_key_pressed(KEY_SPACE):
		space_down = true
	else:
		space_down = false
	if prev != space_down and space_down:
		if is_playing():
			print("STOP")
			stop()
		else:
			print("PLAY")
			play()
