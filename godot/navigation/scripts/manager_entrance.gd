extends Node

export (float,0,10) var distance_max:float = 10
export (float,0,10) var distance_min:float = 0
export (float,0,10) var distance_touch:float = 0

onready var gate = get_parent()
onready var ball = get_node('../ball')
onready var ball_color = ball.material_override.emission

func _ready():
	var ap:AnimationPlayer = get_node("../anim")
	ap.play("startn")
	ball.pulsing_freq = 0
	ball.material_override.emission = Color.black

func _process(delta):
	
	if gate.player != null:
		var diff:Vector3 = gate.player.global_transform.origin - ball.global_transform.origin
		if diff.length() <= distance_touch:
			gate.do_teleport()
		var dratio = min(1,max(0,(diff.length()-distance_min) / (distance_max-distance_min)))
		ball.pulsing_freq = pow((1-dratio),2) * 100
		if dratio >= 1:
			ball.material_override.emission = Color.black
		else:
			ball.material_override.emission = ball_color
