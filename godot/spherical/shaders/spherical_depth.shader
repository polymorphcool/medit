shader_type spatial;
render_mode blend_mix,depth_draw_opaque,cull_front,diffuse_burley,specular_schlick_ggx;
uniform vec4 albedo : hint_color;
uniform sampler2D texture_albedo : hint_albedo;
uniform float specular;
uniform float metallic;
uniform float roughness : hint_range(0,1);
uniform float point_size : hint_range(0,128);
uniform vec3 uv1_scale;
uniform vec3 uv1_offset;
uniform vec3 uv2_scale;
uniform vec3 uv2_offset;

uniform sampler2D texture_depth : hint_black;
uniform mat3 kernel;
uniform float depth;
uniform float depth_power;
uniform float depth_smooth;
uniform float normal_smooth;

uniform sampler2D texture_noise : hint_black;
uniform float noise;
uniform float noise_speed;

float get_depth( sampler2D tex, vec2 uv ) {
	
	float d = 0.0;
	float div = kernel[0][0]+kernel[0][1]+kernel[0][2]+kernel[1][0]+kernel[1][1]+kernel[1][2]+kernel[2][0]+kernel[2][1]+kernel[2][2];
	
	d += texture( tex, uv + vec2( -depth_smooth, -depth_smooth ) ).r * kernel[0][0] / div;
	d += texture( tex, uv + vec2( 0.0, -depth_smooth ) ).r * kernel[0][1] / div;
	d += texture( tex, uv + vec2( depth_smooth, -depth_smooth ) ).r * kernel[0][2] / div;
	
	d += texture( tex, uv + vec2( -depth_smooth, 0.0 ) ).r * kernel[1][0] / div;
	d += texture( tex, uv + vec2( 0.0, 0.0 ) ).r * kernel[1][1] / div;
	d += texture( tex, uv + vec2( depth_smooth, 0.0 ) ).r * kernel[1][2] / div;
	
	d += texture( tex, uv + vec2( -depth_smooth, depth_smooth ) ).r * kernel[2][0] / div;
	d += texture( tex, uv + vec2( 0.0, depth_smooth ) ).r * kernel[2][1] / div;
	d += texture( tex, uv + vec2( depth_smooth, depth_smooth ) ).r * kernel[2][2] / div;
	
	return pow( d * depth, depth_power );
	
	// vec4 accel = texture( texture_accel, vec2( d, 0.0 ) );
	// return ( pow( d, accel.r ) - 1.0 ) * depth;
}

void vertex() {
	UV=UV*uv1_scale.xy+uv1_offset.xy;
	vec2 nuv=UV*uv2_scale.xy+( uv2_offset.xy * vec2(TIME,TIME) * noise_speed );
	float d = get_depth( texture_depth, UV );
	float n = ( -0.5 + texture( texture_noise, nuv ).r ) * noise;
	VERTEX += NORMAL * d + n;
	vec2 e = vec2(normal_smooth, 0.0);
//	vec3 normal = normalize(
//		vec3(
//			get_depth( texture_depth, UV - e ) - get_depth( texture_depth, UV + e ), 
//			2.0 * e.x, 
//			get_depth( texture_depth, UV - e.yx ) - get_depth( texture_depth, UV + e.yx )
//			));
//	NORMAL = normal;
}

void fragment() {
	vec2 base_uv = UV;
	vec4 albedo_tex = texture(texture_albedo,base_uv);
	vec4 depth_tex = texture(texture_depth,base_uv);
	ALBEDO = albedo.rgb * albedo_tex.rgb;
	METALLIC = metallic;
	ROUGHNESS = roughness;
	SPECULAR = specular;
}
