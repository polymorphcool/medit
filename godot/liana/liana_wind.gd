tool

extends Spatial

export (NodePath) var girouette:NodePath = "" setget set_girouette
export (float,0,1000) var wind_min:float = 10
export (float,0,1000) var wind_max:float = 200
export (bool) var blow:bool = true


func set_girouette( np:NodePath ):
	girouette = np
	initialised = false

var initialised:bool = false
var reset:bool = false
var wind_dir:Spatial = null

func _ready():
	pass

func _process(delta):
	
	if !initialised:
		initialised = true
		wind_dir = get_node( girouette )
	
	if blow and wind_dir != null:
		reset = true
		for c in get_children():
			c.wind = wind_dir.global_transform.basis.z * rand_range( wind_min,wind_max )
	elif reset:
		reset = false
		for c in get_children():
			c.wind = Vector3.ZERO
		
