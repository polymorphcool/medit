## installation

git clone git@gitlab.com:polymorphcool/medit.git
git submodule init
git submodule update

to run DenseDepth, see densedepth_installation_notes

## densedepth

by default, demo.py crop images! resizing to 4:3 the input image avoid this effect
