extends AnimationPlayer

export (bool) var loop:bool = true

func _ready():
	var l:PoolStringArray = get_animation_list()
	if len(l) > 0:
		play(l[0])
	connect( "animation_finished", self, "restart" )

func restart( s:String ):
	if !loop:
		return
	play(s)
