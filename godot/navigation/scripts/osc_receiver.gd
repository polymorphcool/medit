extends Node

export (int) var port:int = 25000

var receiver = null

func _ready():
	receiver = load("res://addons/gdosc/gdoscreceiver.gdns").new()
	receiver.setup( port )
	receiver.start()

func _process(delta):
	
	while( receiver.has_message() ):
		var msg = receiver.get_next()
		if msg["address"] == "/player":
			var pos = Vector3( msg["args"][0], msg["args"][1], msg["args"][2] )
			var rot = Vector3( msg["args"][3], msg["args"][4], msg["args"][5] )
			get_parent().set_not_me( pos, rot )
		elif msg["address"] == "/scene":
			var pid = msg["args"][0]
			var path = msg["args"][1]
			get_parent().update_tree( pid, path )
