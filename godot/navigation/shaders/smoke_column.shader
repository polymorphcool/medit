shader_type spatial;
render_mode blend_mix,depth_draw_alpha_prepass,cull_front,diffuse_burley,specular_schlick_ggx,unshaded;
uniform vec4 albedo : hint_color;
uniform sampler2D texture_albedo : hint_albedo;
uniform float specular;
uniform float metallic;
uniform float roughness : hint_range(0,1);
uniform float point_size : hint_range(0,128);
uniform vec3 uv1_scale;
uniform vec3 uv1_offset;
uniform vec3 uv2_scale;
uniform vec3 uv2_offset;

uniform float min_y = 0.0;
uniform float max_y = 1.0;
uniform float speed_up;
uniform float speed_radial;

varying float AMULT;

void vertex() {
	vec2 uvc = UV.xy;
	UV=UV*uv1_scale.xy+uv1_offset.xy;
	UV.y += TIME*speed_up;
	UV.x += uvc.y * speed_radial;
	AMULT = 1.0 - dot(NORMAL,vec3(0.0,1.0,0.0));
	AMULT *= max(0.0,min(1.0,(uvc.y-min_y)/(max_y-min_y)));
}

void fragment() {
	vec2 base_uv = UV;
	vec4 albedo_tex = texture(texture_albedo,base_uv);
	ALBEDO = albedo.rgb * albedo_tex.rgb;
	METALLIC = metallic;
	ROUGHNESS = roughness;
	SPECULAR = specular;
	ALPHA = albedo.a * albedo_tex.r * AMULT;
}
