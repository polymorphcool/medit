tool

extends Spatial

export (Texture) var RGB = null
export (Texture) var DEPTH = null
export (Vector3) var mesh_scale = Vector3(1,1,1)
export (int, 1, 20) var sampling = 10
export (bool) var _generate = false setget generate

func generate( b ):
	_generate = false
	if not b or RGB == null or DEPTH == null:
		return
	
	var dsize = DEPTH.get_size()
	var im = DEPTH.get_data()
	im.lock()
	var vertices = []
	var uvs = []
	var halfsize = Vector3( dsize.x - 1, dsize.y - 1, 1 ) * 0.5
	for y in range( 0, dsize.y ):
		for x in range( 0, dsize.x ):
			var c = im.get_pixel( x, y )
			vertices.append( ( Vector3( x, y, c.gray() ) - halfsize ) * mesh_scale )
			uvs.append( Vector2( x / dsize.x, y / dsize.y ) )
	
#	_surf.clear()
#	_surf.begin( Mesh.PRIMITIVE_TRIANGLES )
	
	var edg0
	var edg1
	var norm

	var _mesh = Mesh.new()
	var _surf = SurfaceTool.new()
	
	_surf.begin( Mesh.PRIMITIVE_TRIANGLES )

	for y in range( 0, dsize.y - 1 ):
		for x in range( 0, dsize.x - 1 ):

			var ids = [ 
				x + y * dsize.x, 
				(x + 1) + y * dsize.x,
				(x + 1) + (y + 1 ) * dsize.x,
				x + (y + 1 ) * dsize.x 
			]
			
			edg0 = ( vertices[ids[0]] - vertices[ids[1]] ).normalized()
			edg1 = ( vertices[ids[0]] - vertices[ids[2]] ).normalized()
			norm = edg1.cross( edg0 )
			
			_surf.add_normal( norm )
			_surf.add_uv( uvs[ids[0]] )
			_surf.add_vertex( vertices[ids[0]] )
			_surf.add_normal( norm )
			_surf.add_uv( uvs[ids[1]] )
			_surf.add_vertex( vertices[ids[1]] )
			_surf.add_normal( norm )
			_surf.add_uv( uvs[ids[2]] )
			_surf.add_vertex( vertices[ids[2]] )
			
			edg0 = ( vertices[ids[2]] - vertices[ids[3]] ).normalized()
			edg1 = ( vertices[ids[2]] - vertices[ids[0]] ).normalized()
			norm = edg1.cross( edg0 )
			
			_surf.add_normal( norm )
			_surf.add_uv( uvs[ids[2]] )
			_surf.add_vertex( vertices[ids[2]] )
			_surf.add_normal( norm )
			_surf.add_uv( uvs[ids[3]] )
			_surf.add_vertex( vertices[ids[3]] )
			_surf.add_normal( norm )
			_surf.add_uv( uvs[ids[0]] )
			_surf.add_vertex( vertices[ids[0]] )
			
	_surf.index()
	_surf.commit( _mesh )
	$mesh.set_mesh( _mesh )
	
	print( "done" )

func _ready():
	#generate( true )
	pass