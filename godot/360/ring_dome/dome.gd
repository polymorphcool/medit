tool

extends Spatial

export(float,0,1) var percent : float = 1
export(float,0,1) var offset : float = 1
export(float,-6.29,6.29) var rot_y : float = 0
export(Vector3) var full_scale : Vector3 = Vector3.ONE * 5000
export(Vector3) var start_scale_mult : Vector3 = Vector3(1,1,1)
export(float,0,1) var full_scale_percent : float = 1
export(float,0.01,10) var animation : float = 2
export(bool) var hide_at_0 : bool = true setget set_hide_at_0
export(bool) var open : bool = false setget set_open

var rings : Array = []
var rings_visible : bool = true
var curr_time : float = 0
var _mat : Material = null

func set_material( mat : Material ):
	_mat = mat
	collect_rings()

func set_open(b:bool):
	if b != open:
		curr_time = animation
	open = b

func set_hide_at_0(b:bool):
	hide_at_0 = b
	if !hide_at_0 and !rings_visible:
		rings_visible = true
		for c in get_children():
			c.visible = rings_visible

func collect_rings():
	rings = []
	for c in get_children():
		c.visible = rings_visible
		if _mat != null and c.material_override != _mat:
			c.material_override = _mat
		rings.append(c)

func _process(delta):
	
	if rings.empty():
		collect_rings()
		if rings.empty():
			return
	
	if curr_time > 0:
		curr_time -= delta
		if curr_time < 0:
			curr_time = 0
		percent = curr_time / animation
		if open:
			percent = 1 - percent
	
	if percent == 0 and hide_at_0 and rings_visible:
		rings_visible = false
		for c in get_children():
			c.visible = rings_visible
	elif percent > 0 and hide_at_0 and !rings_visible:
		rings_visible = true
		for c in get_children():
			c.visible = rings_visible
	
	var rn : int = rings.size()
	var mult : float = (1+sin(-PI*0.5+PI*percent)) * 0.5
	var multi : float = 1-mult
	var scmult : float = (1+sin(-PI*0.5+PI*min(1,percent/full_scale_percent))) * 0.5
	for i in range(0,rn):
		var target_z = i * (-PI/rn)
		rings[i].rotation.y = ( rot_y / rn )  * i * multi
		rings[i].rotation.z = target_z * mult
		var iom : float = multi
		if percent != 1 and iom < 0.2:
			iom = 0.2
		var ioff : float = 1-i*offset*iom
		rings[i].scale = full_scale * start_scale_mult.linear_interpolate(Vector3.ONE, scmult) * ioff
