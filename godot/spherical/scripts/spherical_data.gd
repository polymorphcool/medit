extends Node

export (NodePath) var vr_cam : NodePath = ""
export (NodePath) var vr_gui : NodePath = ""
export (NodePath) var gui_vp : NodePath = ""
export (NodePath) var gui_text : NodePath = ""

var vr_cam_node : ARVRCamera = null
var vr_cam_prev : Transform
var vr_cam_position_delta : Vector3
var vr_cam_rotation_delta : Quat
var gui_label : Label = null

func _ready():
	
	if vr_cam != "":
		vr_cam_node = get_node( vr_cam )
		vr_cam_prev = vr_cam_node.global_transform
	
	if gui_text != null:
		gui_label = get_node( gui_text )
	
	if vr_gui != "" and gui_vp != "":
		var mat = get_node( vr_gui ).material_override
		mat.albedo_texture = get_node( gui_vp ).get_texture()

func get_rotation_delta():
	return vr_cam_rotation_delta.get_euler() / PI

func looking_up():
	if vr_cam_node == null:
		return 0
	var gt = vr_cam_node.global_transform
	return Vector3.UP.dot( gt.basis.xform( Vector3.FORWARD ) )

func _process(delta):
	
	if vr_cam_node != null:
		var gt = vr_cam_node.global_transform
		vr_cam_position_delta = gt.origin - vr_cam_prev.origin
		vr_cam_rotation_delta = (gt.basis * vr_cam_prev.basis.inverse()).get_rotation_quat()
		vr_cam_prev = gt
		
		if gui_label != null:
#			gui_label.text = str( vr_cam_position_delta.length() ) + "\n"
#			gui_label.text += str( vr_cam_position_delta.x ) + "\n"
#			gui_label.text += str( vr_cam_position_delta.y ) + "\n"
#			gui_label.text += str( vr_cam_position_delta.z ) + "\n"
			gui_label.text = 'rot:' + str( get_rotation_delta().length() ) + '\n'
			gui_label.text += 'up:' + str( looking_up() ) + '\n'
