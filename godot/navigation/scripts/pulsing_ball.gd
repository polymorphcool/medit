tool

extends MeshInstance

export (float,0,100) var pulsing_freq:float = 1

var pulsing_a:float = 0

func _ready():
	pass # Replace with function body.

func _process(delta):
	pulsing_a += delta * pulsing_freq
	material_override.emission_energy = (1 + sin(pulsing_a))*0.5
