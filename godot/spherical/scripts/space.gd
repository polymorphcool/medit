extends MeshInstance

export(float,0,10) var ymult = 1
export(float,0,1) var ymult_smooth = 0.1

export(NodePath) var data_path = ""

var data : Node = null

var curr_depth : float = 0
var trgt_depth : float = 0
var curr_depthpow : float = 10
var trgt_depthpow : float = 0

var original_y : float = 0

func apply_ymult( f ):
	scale.y = f
	material_override.set_shader_param( "uv1_scale", Vector3(1,f,1) )
	material_override.set_shader_param( "uv1_offset", Vector3(1,(1-f)*0.5,1) )

func _ready():
	
	trgt_depth = material_override.get_shader_param( "depth" )
	trgt_depthpow = material_override.get_shader_param( "depth_power" )
	original_y = self.transform.origin.y
	
	ymult = 1
	apply_ymult( 1 )
	
	if data_path != "":
		data = get_node( data_path )

func _process(delta):
	
	if curr_depth != trgt_depth:
		if curr_depth < trgt_depth:
			curr_depth += delta
		elif curr_depth > trgt_depth:
			curr_depth -= delta
		if abs(curr_depth-trgt_depth) < 1e-4:
			curr_depth = trgt_depth
		material_override.set_shader_param( "depth", curr_depth )
	
	if curr_depthpow != trgt_depthpow:
		if curr_depthpow < trgt_depthpow:
			curr_depthpow += delta
		elif curr_depthpow > trgt_depthpow:
			curr_depthpow -= delta
		if abs(curr_depthpow-trgt_depthpow) < 1e-4:
			curr_depthpow = trgt_depthpow
		material_override.set_shader_param( "depth_power", curr_depthpow )
	
	if scale.y != ymult:
		scale.y += ( ymult - scale.y ) * delta * ymult_smooth
		apply_ymult( scale.y )
	
	if data != null:
		
		var rotd = data.get_rotation_delta()
		var rotl = rotd.length()
		material_override.set_shader_param( "noise", rotl * 10 )
		var up = data.looking_up()
		if ymult < 100 and up > 0.6:
			ymult += 2.5 * delta
		elif ymult > 1:
			ymult -= 12 * delta
			if ymult < 1:
				ymult = 1
		
		if up > 0.75:
			self.transform.origin.y -= delta * 2 * (up-0.75) / 0.25
		elif up < -0.75:
			self.transform.origin.y -= delta * 2 * (up+0.75) / 0.25
		else:
			self.transform.origin.y += ( original_y - self.transform.origin.y ) * 2 * delta
