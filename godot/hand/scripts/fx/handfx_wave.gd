extends "handfx_common.gd"

export (Vector3) var wave_dir:Vector3 = Vector3(0,0,1) setget set_wave_dir
export (bool) var wave_local:bool = false
export (float,0,10) var wave_freq:float = 1
export (float,0,50) var wave_ampl:float = 1
export (float,-5,5) var wave_offset:float = 0

var offsets:Array = []
var rands:Array = []
var wave_a:float = 0

func set_wave_dir( v:Vector3 ):
	wave_dir = v.normalized()

func _ready():
	fx_initialise()
	if fx_ready():
		for t in trackers:
			randomize()
			offsets.append( rand_range(-PI,PI) )

func _process(delta):
	if !fx_ready():
		return
	wave_a += delta * wave_freq
	for i in range(0,len(trackers)):
		if !mask[i]:
			continue
		var t = trackers[i]
		var wmult = sin( wave_a + offsets[i] ) * wave_ampl + wave_offset 
		var d:Vector3 = wave_dir
		if wave_local:
			d = transforms[i].basis.xform(d).normalized()
		d *= wmult
		if previous[i] != null:
			t.translation += d - previous[i]
		previous[i] = d
