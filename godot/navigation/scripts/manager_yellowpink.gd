extends Node

export (NodePath) var environment:NodePath = ""
export (NodePath) var pastille:NodePath = ""
export (NodePath) var column:NodePath = ""
export (float) var min_energy:float = 0.3
export (float) var far_radius:float = 2
export (float) var near_radius:float = 0.3

export (Color) var pad_off:Color = Color(0,0,0,1)
export (Color) var pad_on:Color = Color(1,1,1,1)

onready var gate:Spatial = get_parent()
onready var env:Environment = get_node( environment ).environment
onready var pad:MeshInstance = get_node( pastille )
onready var col:MeshInstance = get_node( column )

var teleport_delay:float = 1
var teleport_time:float = 0

func _ready():
	if env != null:
		env.background_energy = min_energy
	if pad != null:
		pad.material_override.albedo_color = pad_off

func _process(delta):
	
	if env == null or pad == null:
		return
	
	var pp = pad.to_local(gate.player.global_transform.origin*Vector3(1,0,1))
	var l = pp.length()
	if l > far_radius:
		env.background_energy = min_energy
	else:
		var pc = max(0.0,min(1.0,((l-near_radius)/(far_radius-near_radius))))
		env.background_energy = min_energy + (1+sin(-PI*0.5+(1-pc)*PI)) * 0.5 * (1-min_energy)
		if pc == 0:
			pad.material_override.albedo_color = pad_on
			teleport_time += delta
			if teleport_time >= teleport_delay:
				gate.do_teleport()
		elif teleport_time != 0:
			pad.material_override.albedo_color = pad_off
			teleport_time = 0
