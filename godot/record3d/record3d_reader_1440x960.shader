shader_type spatial;
render_mode blend_mix,depth_draw_opaque,cull_disabled,diffuse_burley,specular_schlick_ggx,unshaded;
uniform vec4 albedo : hint_color;
uniform sampler2D texture_albedo : hint_albedo;
uniform float specular;
uniform float metallic;
uniform float roughness : hint_range(0,1);

uniform float near = 2.0;
uniform float far = 5.0;
uniform float search_range : hint_range(0,0.1);
uniform float max_slope : hint_range(0,5);

varying float keep;

float extract_depth( vec3 rgb ) {
	float d = 0.0;
	if (rgb.g >= 1.0 ) {
		d = rgb.g + rgb.r * near;
	} else {
		d = rgb.g + rgb.b - rgb.r * far;
	}
	return d;
}

float around( vec2 uv, vec2 gap ) {
	float curr = extract_depth( texture(texture_albedo,uv).rgb );
	vec2 above_uv = vec2( uv.x, max(0.0, uv.y - gap.y ) );
	if ( abs(curr - extract_depth( texture(texture_albedo,above_uv).rgb ) ) > max_slope ) {
		return 0.0;
	}
	vec2 below_uv = vec2( uv.x, min(1.0, uv.y + gap.y ) );
	if ( abs(curr - extract_depth( texture(texture_albedo,below_uv).rgb ) ) > max_slope ) {
		return 0.0;
	}
	vec2 left_uv = vec2( max(0.0, uv.x - gap.x ), uv.y );
	if ( abs(curr - extract_depth( texture(texture_albedo,left_uv).rgb ) ) > max_slope ) {
		return 0.0;
	}
	vec2 right_uv = vec2( min(1.0, uv.x + gap.x ), uv.y );
	if ( abs(curr - extract_depth( texture(texture_albedo,right_uv).rgb ) ) > max_slope ) {
		return 0.0;
	}
	return 1.0;
}

void vertex() {
	// getting depth on the left side of the image
	vec2 depthuv = UV * vec2(0.5,1.0);
	VERTEX += vec3(0.0, 1.1, 0.0) * extract_depth( texture(texture_albedo,depthuv).rgb );
	keep = around( depthuv, vec2(search_range) );
}

void fragment() {
	// getting the colors on the right side of the image
	vec2 coloruv = ( UV - vec2(1.0,0.0) ) * vec2(0.5,1.0);
	vec4 albedo_tex = texture(texture_albedo,coloruv);
	ALBEDO = albedo.rgb * albedo_tex.rgb;
	METALLIC = metallic;
	ROUGHNESS = roughness;
	SPECULAR = specular;
	ALPHA = keep;
	ALPHA_SCISSOR = 0.9;
}
