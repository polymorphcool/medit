extends MeshInstance

export(NodePath) var copy_position : NodePath = ""
var origin : Spatial = null

func _ready():
	origin = get_node(copy_position)
	$dome.set_material( material_override )
	$dome.open = true

# warning-ignore:unused_argument
func _process(delta):
	if origin == null:
		return
	global_transform.origin = origin.global_transform.origin

func _input(event):
	
	if event is InputEventKey and event.pressed:
		if event.scancode == KEY_RIGHT:
			$dome.open = true
		elif event.scancode == KEY_LEFT:
			$dome.open = false
