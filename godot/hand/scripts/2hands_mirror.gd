tool

extends Spatial

var fingers:Array = []

func _ready():
	pass # Replace with function body.

func _process(delta):
	
	if not visible:
		return
	
	if fingers.empty():
		fingers.append( [ get_node("ctrl/001"), 	get_node("hand4/model/trackers/thumb"), 	get_node("hand5/model/trackers/pinky") ] )
		fingers.append( [ get_node("ctrl/002"), 	get_node("hand4/model/trackers/index"), 	get_node("hand5/model/trackers/ring") ] )
		fingers.append( [ get_node("ctrl/003"), 	get_node("hand4/model/trackers/middle"), 	get_node("hand5/model/trackers/middle") ] )
		fingers.append( [ get_node("ctrl/004"), 	get_node("hand4/model/trackers/ring"), 		get_node("hand5/model/trackers/index") ] )
		fingers.append( [ get_node("ctrl/005"), 	get_node("hand4/model/trackers/pinky"), 	get_node("hand5/model/trackers/thumb") ] )
	for f in fingers:
		f[1].global_transform.origin = f[0].global_transform.origin
		f[2].global_transform.origin = f[0].global_transform.origin
