extends Spatial

export (NodePath) var point_to:NodePath = ""
export (float,0,10) var slerp:float = 3
export (float,0,100) var near_distance:float = 10
export (float,0,100) var far_distance:float = 30
export (float,0,100) var dist_power:float = 5

export (float,0,1) var thumb_mult:float = 0.3
export (float,0,1) var index_mult:float = 0.3
export (float,0,1) var major_mult:float = 0.3
export (float,0,1) var ring_mult:float = 0.3
export (float,0,1) var pinky_mult:float = 0.3


var initialised:bool = false
var tracked:Spatial = null
onready var trackers:Array = $hand.get_trackers()
onready var origins:Array = $origins.get_children()
onready var multipilers:Array = [thumb_mult,index_mult,major_mult,ring_mult,pinky_mult]

func _ready():
	pass # Replace with function body.

func _process(delta):
	
	if !initialised:
		initialised = true
		tracked = get_node( point_to )
		for i in range(0,5):
			var t = trackers[i+1]
			var o = origins[i]
			o.global_transform = t.global_transform
		
	if tracked != null:
		
		var diff:Vector3 = (tracked.global_transform.origin - global_transform.origin) * Vector3(1,0,1)	
		var s:float = min( 1, slerp*delta )
		var front:Vector3 = global_transform.basis.z*(1-s) + diff.normalized()*s
		var b:Basis = Basis( front.cross(Vector3.UP), Vector3.UP, front )
		global_transform.basis = b
		
		for i in range(0,5):
			var t = trackers[i+1]
			var o = origins[i]
			var offset = tracked.global_transform.origin - o.global_transform.origin
			var l:float = offset.length()
			if l < far_distance:
				var pc = pow( 1.0 - max(0, (l-near_distance) / (far_distance-near_distance) ), dist_power )
				t.global_transform.origin = o.global_transform.origin + offset * pc * multipilers[i]
			else:
				t.global_transform.origin = o.global_transform.origin
