tool

extends Node

export (NodePath) var root_path:NodePath = ""
export (NodePath) var mixer_path:NodePath = "" setget set_mixer_path
export (String) var export_folder:String = "res://hand/poses/"
export (String) var pose_name:String = "" setget set_pose_name
export (bool) var record:bool = false setget do_record
export (Array) var poses:Array = []

var root_node:Spatial = null
var mixer_node:Spatial = null
var pose:Array = []

func set_mixer_path( np:NodePath ):
	mixer_path = np
	mixer_node = get_node( mixer_path )
	if not mixer_node == null and not mixer_node.has_method( 'refresh_poses' ):
		mixer_node = null

func set_pose_name( s:String ):
	pose_name = s
	if pose_name != "" && not pose_name.ends_with(".json"):
		pose_name += ".json"

func add_pose( path:String ):
	var node:Spatial = root_node.get_node( path )
	if node == null:
		printerr( "No node found for ", path )
		return
	pose.append({
		'node': path,
		'pos': [node.translation.x,node.translation.y,node.translation.z],
		'rot': [node.rotation.x,node.rotation.y,node.rotation.z],
		'sca': [node.scale.x,node.scale.y,node.scale.z]
	})

func do_record( b:bool ):
	record = false
	if b:
		if pose_name == "":
			printerr( "Set a pose name to save current pose!" )
			return
		if root_node == null:
			root_node = get_node( root_path )
			if root_node == null:
				printerr( "Set a root node before saving poses!" )
				return
		pose = []
		add_pose( "model/trackers" )
		add_pose( "model/trackers/thumb" )
		add_pose( "model/trackers/index" )
		add_pose( "model/trackers/middle" )
		add_pose( "model/trackers/ring" )
		add_pose( "model/trackers/pinky" )
		var file = File.new()
		file.open( export_folder + pose_name, File.WRITE)
		file.store_string(to_json(pose))
		file.close()
		refresh_poses()
		print( "pose successfully saved in ", export_folder + pose_name )

func refresh_poses():
	poses = []
	var dir = Directory.new()
	dir.open(export_folder)
	dir.list_dir_begin()
	while true:
		var file = dir.get_next()
		if file == "":
			break
		elif file.ends_with(".json"):
			poses.append(file)
	poses.sort()
	dir.list_dir_end()
	if mixer_node != null:
		mixer_node.refresh_poses()

func _ready():
	if Engine.editor_hint: 
		refresh_poses()
