tool

extends Spatial

export (bool) var reset:bool = false setget do_reset
export (bool) var waving:bool = false setget set_waving
export (float) var waving_interpol:float = 5 setget set_waving_interpol
export (Vector3) var wave_dir:Vector3 = Vector3(0,0,1) setget set_wave_dir
export (bool) var wave_local:bool = false
export (float,0,10) var wave_freq:float = 1
export (float,0,50) var wave_ampl:float = 1
export (float,-5,5) var wave_offset:float = 0

export (bool) var shuffle:bool = false setget set_shuffle

export (NodePath) var touch_target:NodePath = "" setget set_touch_target
export (bool) var touching:bool = false setget set_touching
export (float) var touching_interpol:float = 5 setget set_touching_interpol
export (float,0,1) var touching_offset:float = 0.3

var trackers:Array = []
var tracker_scaned:bool = false
var wave_a:float = 0
var touch_node:Spatial = null

func set_waving( b:bool ):
	waving = b
	if not waving:
		do_reset(true)
	else:
		set_touching(false)
		$model/ccdik._interpolation_speed = waving_interpol

func set_waving_interpol( f:float ):
	waving_interpol = f
	if waving:
		$model/ccdik._interpolation_speed = waving_interpol

func set_touching( b:bool ):
	touching = b
	if Engine.editor_hint:
		PhysicsServer.set_active( touching )
	if not touching:
		do_reset(true)
	else:
		set_waving(false)
		$model/ccdik._interpolation_speed = touching_interpol

func set_touching_interpol( f:float ):
	touching_interpol = f
	if touching:
		$model/ccdik._interpolation_speed = touching_interpol

func set_touch_target( np:NodePath ):
	touch_target = np
	if np == "":
		touch_node = null
	else:
		touch_node = get_node( np )
		if not touch_node is RigidBody:
			touch_node = null

func set_shuffle( b:bool ):
	shuffle = false
	if b:
		randomize()
		for t in trackers:
			t.rand = randf()

func set_wave_dir( v:Vector3 ):
	wave_dir = v
	wave_dir = wave_dir.normalized()

func do_reset(b:bool):
	reset = false
	if b and $model != null:
		$model/ccdik/thumb.target2bone()
		$model/ccdik/index.target2bone()
		$model/ccdik/middle.target2bone()
		$model/ccdik/ring.target2bone()
		$model/ccdik/pinky.target2bone()
		for t in trackers:
			if t.ray != null:
				remove_child( t.ray )
		trackers = []
		tracker_scaned = false

func add_tracker( t:Spatial, parent:Spatial ):
	randomize()
	var tr:Dictionary = {
		'obj': t,
		'parent': parent,
		'glob': Transform(t.global_transform),
		'glob_pos': Vector3(t.global_transform.origin),
		'loc': Transform(t.transform),
		'loc_pos': Vector3(t.transform.origin),
		'loc_sca': t.transform.basis.get_scale(),
		'ray': null,
		'rand': randf()
	}
	if parent == null:
		t.transform.basis = Basis(Vector3(-1,0,0),Vector3(0,1,0),Vector3(0,0,-1))
	trackers.append(tr)

func scan_trackers():
	if $model == null:
		return
	if trackers.empty():
		add_tracker( $model/trackers,			null )
		add_tracker( $model/trackers/thumb,		$model/trackers )
		add_tracker( $model/trackers/index,		$model/trackers )
		add_tracker( $model/trackers/middle,	$model/trackers )
		add_tracker( $model/trackers/ring,		$model/trackers )
		add_tracker( $model/trackers/pinky,		$model/trackers )
	tracker_scaned = true

func wave_tracker():
	for t in trackers:
		if !t.parent:
			continue
		var sc = wave_offset + sin( wave_a + t.rand * PI ) * wave_ampl
		var d = wave_dir
		if wave_local:
			d = t.glob.basis.xform( d ).normalized()
		t.obj.transform.origin = t.loc_pos + d * sc

func touch_tracker():
	
	if touch_node == null:
		return
	
	var tb = touch_node.global_transform.basis
	var to = touch_node.global_transform.origin
	var pr = null
	for t in trackers:
		if !t.parent:
			pr = t.obj.global_transform.looking_at(to,tb.y)
#			t.obj.global_transform = t.obj.global_transform.looking_at(to,tb.y)
			continue
		if pr == null:
			continue
		if t.ray == null:
			var r = RayCast.new()
			add_child( r )
			r.name = t.obj.name + ".ray"
			r.collide_with_bodies = true
			t.ray = r
		var offset = pr.xform(t.loc_pos-trackers[2].loc_pos) - pr.origin
		var o = pr.xform(t.loc_pos)
		t.ray.global_transform.origin = o
		t.ray.cast_to = to + offset * touching_offset - o
		t.ray.force_raycast_update()
		if t.ray.is_colliding():
			t.obj.global_transform.origin = t.ray.get_collision_point()

func reset_trackers():
	for t in trackers:
		t.obj.global_transform = t.glob
		t.obj.scale = Vector3.ONE

func _ready():
	do_reset(true)
	set_touch_target( touch_target )

func _process(delta):
	
	if !visible:
		return
	
	if !tracker_scaned:
		scan_trackers()
	
	if waving:
		wave_a += delta * wave_freq
		wave_tracker()
	
	elif touching:
		touch_tracker()
	
