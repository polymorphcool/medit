extends Node

export (float,0,200) var time_out:float = 10
export (NodePath) var environment:NodePath = ""
export (NodePath) var light:NodePath = ""
export (Color) var start_color:Color = Color(0,0,0,1)
export (Color) var end_color:Color = Color(1,1,1,1)

onready var gate:Spatial = get_parent()
onready var env:Environment = get_node( environment ).environment
onready var spot:Light = get_node( light )
onready var time:float = time_out

func _ready():
	if env != null:
		env.background_color = start_color
	if spot != null:
		spot.light_color = end_color * 0.5 + Color(0.5,0.5,0.5,0.0)

func _process(delta):
	
	if gate == null:
		return
	
	time -= delta
	if time <=0 :
		gate.do_teleport()
	elif env != null:
		var pc = time/time_out
		env.background_color = start_color * pc + end_color * (1-pc)
