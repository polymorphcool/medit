tool

extends MeshInstance

export (NodePath) var pusher_path:NodePath = ""
export (float,0,100) var pusher_radius:float = 1

var pusher:Spatial = null
var materials:Array = []
var initialised:bool = false

func _ready():
	pass

func _process(delta):
	if !initialised:
		initialised = true
		pusher = get_node(pusher_path)
		for i in range(0,mesh.get_surface_count()):
			var m:Material = mesh.surface_get_material(i)
			if m is ShaderMaterial:
				materials.append( m )
	if pusher != null:
		pass
	var locp = to_local( pusher.global_transform.origin )
	for m in materials:
		m.set_shader_param( "pusher_pos", locp )
		m.set_shader_param( "pusher_rad", pusher_radius )
