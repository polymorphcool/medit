extends "handfx_common.gd"

export (NodePath) var thumb_target:NodePath = ""
export (NodePath) var index_target:NodePath = ""
export (NodePath) var middle_target:NodePath = ""
export (NodePath) var ring_target:NodePath = ""
export (NodePath) var pinky_target:NodePath = ""

var targets:Array = []

func _ready():
	fx_initialise()
	for i in range(0,len(trackers)):
		var n:Spatial = null 
		if i == 1:
			n = get_node(thumb_target)
			if n == null:
				printerr( "Invalid thumb_target " + thumb_target )
		elif i == 2:
			n = get_node(index_target)
			if n == null:
				printerr( "Invalid index_target " + index_target )
		elif i == 3:
			n = get_node(middle_target)
			if n == null:
				printerr( "Invalid middle_target " + middle_target )
		elif i == 4:
			n = get_node(ring_target)
			if n == null:
				printerr( "Invalid ring_target " + ring_target )
		elif i == 5:
			n = get_node(pinky_target)
			if n == null:
				printerr( "Invalid pinky_target " + pinky_target )
		targets.append(n)

# warning-ignore:unused_argument
func _process(delta):
	if !fx_ready():
		return
	for i in range(0,len(trackers)):
		if !mask[i] or targets[i] == null:
			continue
		var trck = trackers[i]
		var tgt = targets[i]
		trck.global_transform.origin = tgt.global_transform.origin
