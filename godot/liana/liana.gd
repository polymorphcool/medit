tool

extends ImmediateGeometry

export (NodePath) var target_start:NodePath = "" setget set_target_start
export (NodePath) var target_end:NodePath = "" setget set_target_end
export (int,1,50) var iterations:int = 30
export (int,2,100) var splits:int = 20 setget set_splits
export (float,0,100) var damping:float = 0.1
export (float,0,100) var stiffness:float = 0.5
export (float,0,100) var weight:float = 1
export (float,-10,10) var contraction:float = 0 setget set_contraction
export (bool) var end_attached:bool = true
export (Vector3) var wind:Vector3 = Vector3.ZERO
export (bool) var play:bool = false

func set_target_start( np:NodePath ):
	target_start = np
	initialised = false

func set_target_end( np:NodePath ):
	target_end = np
	initialised = false

func set_splits( i:int ):
	splits = i
	initialised = false

func set_contraction( f:float ):
	contraction = f
	if initialised:
		for s in segments:
			s.length = pow(s.init_length - s.init_length * contraction,2)

var initialised:bool = false
var start:Spatial = null
var end:Spatial = null
var prev_start:Vector3 = Vector3.ZERO
var prev_end:Vector3 = Vector3.ZERO
var segments:Array = []

func _ready():
	pass

func add_segment( pos:Vector3, length:float, percent:float ):
	var prevs = null
	if segments.size() > 0:
		prevs = segments[segments.size()-1]
	var news = {
		'pos': pos,
		'force': Vector3.ZERO,
		'init_length': length,
		'length': pow(length - length * contraction,2),
		'pc': percent,
		'previous': prevs,
		'next': null
	}
	if prevs != null:
		prevs.next = news
	segments.append( news )

func _process(delta):
	
	if !initialised:
		initialised = true
		start = get_node( target_start )
		end = get_node( target_end )
		if start == null or not start is Spatial or end == null or not end is Spatial:
			start = null
			end = null
		else:
			segments = []
			var p0 = to_local(start.global_transform.origin)
			var p1 = to_local(end.global_transform.origin)
			prev_start = p0
			prev_end = p1
			var diff = p1 - p0
			var length = diff.length() / splits
			for i in range(0,splits+1):
				add_segment(p0,length, i*1.0 / splits)
				p0 += diff / splits
	
	if start == null:
		return
		
	var p0 = to_local(start.global_transform.origin)
	var p1 = to_local(end.global_transform.origin)
	var diff0:Vector3 = p0 - prev_start
	var diff1:Vector3 = p1 - prev_end
	prev_start = p0
	prev_end = p1
	
	if play:
		for s in segments:
			s.force += diff0 * s.pc
			s.force += diff1 * (1-s.pc)
		var m:float = 1.0 / iterations
		var subdelta:float = delta / iterations
		var diff:Vector3 = Vector3.ZERO
		var force:Vector3 = Vector3.ZERO
		for i in range(0,iterations):
			for s in segments:
				if s.previous == null:
					s.pos = p0
					continue 
				if s.next == null and end_attached:
					s.pos = p1
					continue
				s.force += Vector3.DOWN * 9.1 * weight * subdelta
				s.force += wind * subdelta
				if s.next != null:
					diff = s.next.pos - s.pos
					force = diff.normalized() * (diff.length_squared() - s.length)
					s.force += force * weight * stiffness
				diff = s.pos - s.previous.pos
				force = diff.normalized() * (diff.length_squared() - s.length)
				s.force -= force * weight * stiffness
			for s in segments:
				if s.previous != null and (s.next != null or !end_attached): 
					var f = s.force * subdelta
					s.pos += f
					s.force -= f
					s.force -= s.force * damping * subdelta
	
	clear()
	begin( Mesh.PRIMITIVE_LINES )
	var i = 0
	for s in segments:
		if s.next != null:
			if i%2 == 0:
				set_color( Color( 0,1,1 ) )
			else:
				set_color( Color( 1,0,0 ) )
			add_vertex( s.pos )
			add_vertex( s.next.pos )
		i += 1
	end()
