extends Node

onready var gate:Spatial = get_parent()
onready var pastille_floor:Spatial = get_node( "../floor" )

func _ready():
	pass

func _process(delta):
	
	if gate.player != null:
		pastille_floor.player_pos = pastille_floor.to_local( gate.player.global_transform.origin )
